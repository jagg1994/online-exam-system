<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presults', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enrol_id')->index()->unsigned();
            $table->foreign('enrol_id')->references('id')->on('enrols')->onDelete('cascade');
            $table->time('time_taken');
            $table->integer('marks');
            $table->integer('island_rank');
            $table->integer('district_rank');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presults');
    }
}

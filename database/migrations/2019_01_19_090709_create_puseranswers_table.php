<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuseranswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puseranswers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enrol_id')->unsigned()->index();
            $table->foreign('enrol_id')->references('id')->on('enrols')->onDelete('cascade');   
            $table->integer('question_id')->unsigned()->index();
            $table->foreign('question_id')->references('id')->on('pquestions')->onDelete('cascade');
            $table->integer('user_answer');
            $table->boolean('correct');
            $table->time('time_taken');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puseranswers');
    }
}

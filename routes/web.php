<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/newcategory', function () {
    return view('newcategory');
});
Route::post('/saveCategory', 'CatergoryController@store');

Route::get('/newsubject', function () {
    return view('newsubject');
});
Route::post('/saveSubject', 'SubjectController@store');

Route::get('/newpaper', function () {
    $datas = App\subject::all();
    $datac = App\category::all();

    return view('newpaper')->with('subject',$datas)->with('category',$datac);
});
Route::post('/savepaper', 'PaperController@store');


Route::get('/newquestion', function () {
    return view('newquestion');
});
Route::post('/savequestion', 'PquestionController@store');
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <form method="post" action="/savepaper">
                {{csrf_field()}}
                     <div class="form-group">
                     <label>Exam Name</label>
                    <input type="text" name="name" class="form-control"  placeholder="Enter Name Here"> 
                    </div>
                    <div class="form-group">
                <label>Exam Type</label>
                <select class="form-control" name="category_id">
                @foreach($category as $categories)
                    <option value="{{$categories->id}}">{{$categories->name}}</option>  
                    @endforeach
                </select>
                    </div>
                    <div class="form-group">
                <label>Subject</label>
                <select class="form-control" name="subject_id">
                @foreach($subject as $subjects)
                    <option value="{{$subjects->id}}">{{$subjects->name}}</option>  
                    @endforeach
                </select>
                    </div>
                    <div class="form-group">
                     <label>Duration</label>
                    <input type="text" name="duration" class="form-control"  placeholder="Enter Name Here"> 
                    </div>
                    <div class="form-group">
                     <label>Price</label>
                    <input type="text" name="price" class="form-control"  placeholder="Enter Name Here"> 
                    </div>
                    <div class="form-group">
                    
                     <div class="radio">
                    <label>
                        <input type="radio" name="status" id="optionsRadios1" value="1" checked>Active
                    </label>
                </div>
            </div>
                <input type="submit" class="btn btn-primary" value="Save">
                </form>
            </div>
        </div>
    </div>
</div>